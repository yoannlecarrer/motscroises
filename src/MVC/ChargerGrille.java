package MVC;

import java.sql.*;
import java.util.*;

public class ChargerGrille {

	private Connection connexion;

	public ChargerGrille() {
		try {
			connexion = connecterBD();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Connection connecterBD() throws SQLException
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Driver OK");
	         
		} catch (Exception e) {
			e.printStackTrace();
		} 

		String url = "jdbc:mysql://localhost:3306/tp_prga?user=root";
		//jdbc:mysql://mysql.istic.univ-rennes1.fr/base_bousse

		Connection connect = DriverManager.getConnection(url);
		System.out.println("Connexion effective !");         
    
		return connect ;
	}

	// Retourne la liste des grilles disponibles dans la B.D.
	// Chaque grille est d�crite par la concat�nation des valeurs
	// respectives des colonnes nom_grille, hauteur et largeur.
	// L��l�ment de liste ainsi obtenu est index� par le num�ro de
	// la grille (colonne num_grille).
	// Ainsi "Fran�ais d�butants (7x6)" devrait �tre associ� � la cl� 10
	public Map<Integer, String> grillesDisponibles()
	{ 
		Map<Integer,String> grilles = new HashMap<>();
		
		try {
			ResultSet res = connexion.createStatement().executeQuery("Select * from tp5_grille");
			
			while(res.next()) {
				int num = res.getInt("num_grille");
				String nom = res.getString("nom_grille") + " (" + res.getInt("largeur") + "x" + res.getInt("hauteur") + ")\n";
				grilles.put(num, nom);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return grilles;
	}
	
	public MotsCroises extraireGrille(int numGrille) throws SQLException
	{ 
		MotsCroises mc = new MotsCroises(1,1);
		try {
			PreparedStatement ps = connexion.prepareStatement("Select largeur,hauteur from tp5_grille where num_grille=?");
			ps.setInt(1, numGrille);
			ResultSet res = ps.executeQuery();
			res.next();
			
			mc = new MotsCroises(res.getInt("largeur"),res.getInt("hauteur"));
			
			ps = connexion.prepareStatement("Select * from tp5_mot where num_grille=?");
			ps.setInt(1, numGrille);
			res = ps.executeQuery();
			
			while(res.next()) {
				String def 	= res.getString("definition");
				Boolean hor	= res.getBoolean("horizontal");
				int ligne 	= res.getInt("ligne");
				int colonne = res.getInt("colonne");
				char[] sol	= res.getString("solution").toCharArray();
				
				mc.setDefinition(ligne, colonne, hor, def);
				for(int i=0; i<sol.length; i++) {
					if(hor) {
						mc.setSolution(ligne, colonne+i, sol[i]);
					}else {
						mc.setSolution(ligne+i, colonne, sol[i]);
					}
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mc;
	}
	
	public static void main(String[] args) throws SQLException {
		//ChargerGrille.connecterBD();
		ChargerGrille cg = new ChargerGrille();
		System.out.println(cg.grillesDisponibles());
		try {
			cg.extraireGrille(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
