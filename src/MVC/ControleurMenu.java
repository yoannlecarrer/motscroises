package MVC;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;


public class ControleurMenu {


    @FXML
    private Button bMot;
    @FXML
    private TextField tField;



    @FXML
    public void initialize() {

    }

    public void close(MouseEvent e) {
        if (e.getButton() == MouseButton.PRIMARY) {
            System.exit(0);
        }
    }

    public void motAleatoire(MouseEvent e) throws IOException {
        if (e.getButton() == MouseButton.PRIMARY) {
            Stage stage = (Stage) bMot.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("Vue.fxml"));
            Parent root = (Parent) loader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        }
    }

    public void motChoix2(MouseEvent e) throws IOException {
        if (e.getButton() == MouseButton.PRIMARY) {
            Stage stage = (Stage) bMot.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("VueChoix2.fxml"));
            Parent root = (Parent) loader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        }
    }

    }


