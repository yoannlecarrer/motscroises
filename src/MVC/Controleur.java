package MVC;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;


public class Controleur {

    private MotsCroises mc;
    private ChargerGrille cg;
    private boolean directionCourante = true;
    // directionCourante true => horizontal (vers la droite)
    // directionCourante  false => vertical (vers le bas)


    @FXML
    private GridPane monGridPane;
    @FXML
    private Button bMot;

    @FXML
    public void initialize() throws SQLException {
        cg = new ChargerGrille();
        int nb = genererNumGrille(9);
        System.out.println("Numéro grille : "+nb);
        mc = cg.extraireGrille(nb);

        this.creerGrille();

        for (Node n : monGridPane.getChildren()) {
            if (n instanceof TextField) {
                TextField tf = (TextField) n;
                int lig = ((int) n.getProperties().get("gridpane-row")) + 1;
                int col = ((int) n.getProperties().get("gridpane-column")) + 1;

                tf.textProperty().bindBidirectional(mc.propositionProperty(lig, col));



                String defH = mc.getDefinition(lig, col, true);
                String defV = mc.getDefinition(lig, col, false);

                if (defH != null && defV != null) {
                    tf.setTooltip(new Tooltip(defH + " / " + defV));
                } else if (defH != null) {
                    tf.setTooltip(new Tooltip(defH));
                } else if (defV != null) {
                    tf.setTooltip(new Tooltip(defV));
                }
                tf.setOnMouseClicked((e) -> {
                    this.clicCase(e);
                });

                tf.setOnKeyPressed((e) -> {
                    this.deplacements(e);
                });

                tf.setOnKeyReleased((e) -> {
                    this.remplirProp(e);
                });

            }
        }
    }

    private void creerGrille() {
        TextField modele = (TextField) monGridPane.getChildren().get(0);
        monGridPane.getChildren().clear();

        for (int lig = 1; lig <= mc.getHauteur(); lig++) {
            for (int col = 1; col <= mc.getLargeur(); col++) {
                if (!mc.estCaseNoire(lig, col)) {
                    TextField tf = new TextField();
                    tf.setPrefHeight(modele.getPrefHeight());
                    tf.setPrefWidth(modele.getPrefWidth());
                    //tf.setStyle("-fx-border-color: #8dc5e0 ;-fx-border-width: 2px");
                    for (Object cle : modele.getProperties().keySet()) {
                        tf.getProperties().put(cle, modele.getProperties().get(cle));
                    }
                    monGridPane.add(tf, col - 1, lig - 1);
                }
            }
        }
    }

    @FXML
    public void clicCase(MouseEvent e) {
        if (e.getButton() == MouseButton.SECONDARY) {

            TextField maCase = (TextField) e.getSource();
            int lig = ((int) maCase.getProperties().get("gridpane-row")) + 1;
            int col = ((int) maCase.getProperties().get("gridpane-column")) + 1;
            mc.reveler(lig, col);
            maCase.textProperty().setValue(Character.toString(mc.getProposition(lig, col)).toUpperCase());
        }
    }

    int genererNumGrille(int borneSup) {
        Random r = new Random();
        return 1 + r.nextInt(borneSup - 1);
    }

    public void retour(MouseEvent e) throws IOException {
        if (e.getButton() == MouseButton.PRIMARY) {
            Stage stage = (Stage) bMot.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("VueMenu.fxml"));
            Parent root = (Parent) loader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        }
    }

    public void deplacements(KeyEvent e) {

        TextField maCase = (TextField) e.getSource();
        int lig = ((int) maCase.getProperties().get("gridpane-row"));
        int col = ((int) maCase.getProperties().get("gridpane-column"));

        if (e.getCode() == KeyCode.UP) {
            this.enHaut(lig, col);
            directionCourante = false;
        } else if (e.getCode() == KeyCode.DOWN) {
            this.enBas(lig, col);
            directionCourante = false;
        } else if (e.getCode() == KeyCode.LEFT) {
            this.aGauche(lig, col);
            directionCourante = true;
        } else if (e.getCode() == KeyCode.RIGHT) {
            this.aDroite(lig, col);
            directionCourante = true;
        } else if (e.getCode() == KeyCode.BACK_SPACE) {
            maCase.textProperty().setValue("");
            if(directionCourante){
                this.aGauche(lig,col);
            }else {
                this.enHaut(lig,col);
            }
        }

    }

    private void enHaut(int lig, int col) {
        if (lig != 0) {
            int i = 1;
            TextField tf = (TextField) getTextfield(lig - i, col);
            while (tf == null && (lig - i) != 0) {
                i++;
                tf = (TextField) getTextfield(lig - i, col);
            }
            tf.requestFocus();
        }
    }

    private void enBas(int lig, int col) {
        if (lig != mc.getHauteur() - 1) {
            int i = 1;
            TextField tf = (TextField) getTextfield(lig + i, col);
            while (tf == null && (lig + i) != mc.getHauteur() - 1) {
                i++;
                tf = (TextField) getTextfield(lig + i, col);
            }
            tf.requestFocus();
        }
    }

    private void aGauche(int lig, int col) {
        if (col != 0) {
            int i = 1;
            TextField tf = (TextField) getTextfield(lig, col - 1);
            while (tf == null && (col - 1) != 0) {
                i++;
                tf = (TextField) getTextfield(lig, col - i);
            }
            tf.requestFocus();
        }
    }

    private void aDroite(int lig, int col) {
        if (col != mc.getHauteur() - 1) {
            int i = 1;
            TextField tf = (TextField) getTextfield(lig, col + 1);
            while (tf == null && (col + 1) != mc.getHauteur() - 1) {
                i++;
                tf = (TextField) getTextfield(lig, col + i);
            }
            tf.requestFocus();
        }
    }

    public void remplirProp(KeyEvent e) {

        TextField maCase = (TextField) e.getSource();
        int lig = ((int) maCase.getProperties().get("gridpane-row"));
        int col = ((int) maCase.getProperties().get("gridpane-column"));

        if (e.getCode().isLetterKey()) {
            String prop = e.getText().toUpperCase();
            maCase.textProperty().set(prop);

            if (directionCourante) {
                this.aDroite(lig, col);
            } else {
                this.enBas(lig, col);
            }
        }else{
            maCase.textProperty().set(" ");
    }

}

    private Node getTextfield(int lig, int col) {
        for (Node node : monGridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == lig) {
                return node;
            }
        }
        return null;
    }

}
