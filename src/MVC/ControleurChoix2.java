package MVC;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;


public class ControleurChoix2 {

    private ChargerGrille cg;

    @FXML
    private GridPane monGridPane;
    @FXML
    private Button bMot;
    @FXML
    private ChoiceBox c;

    @FXML
    public void initialize() throws SQLException {
        cg = new ChargerGrille();
        c.getItems().addAll("1", "2", "3", "4", "4", "5", "6", "7", "8", "9","10");
    }


    public void retour(MouseEvent e) throws IOException {
        if (e.getButton() == MouseButton.PRIMARY) {
            Stage stage = (Stage) bMot.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("VueMenu.fxml"));
            Parent root = (Parent) loader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        }
    }

    /*public int valeur(){
        String i = (String) c.getValue();
        return Integer.parseInt(i);
    }*/

    public void motChoix(MouseEvent e) throws IOException {
        if (e.getButton() == MouseButton.PRIMARY) {
            Stage stage = (Stage) bMot.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("VueChoix.fxml"));
            Parent root = (Parent) loader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        }
    }


//Etc.

}
