package MVC;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;
import java.util.Scanner;


public class ControleurChoix {

    private MotsCroises mc;
    private ChargerGrille cg;

    @FXML
    private GridPane monGridPane;
    @FXML
    private Button bMot;
    @FXML
    private TextField tField;

    @FXML
    public void initialize() throws SQLException {
        ControleurChoix2 c = new ControleurChoix2();
        cg = new ChargerGrille();
        int nb = choixNumGrille();
        System.out.println("Numéro grille : "+nb);
        mc = cg.extraireGrille(nb);

        this.creerGrille();

        for (Node n : monGridPane.getChildren()) {
            if (n instanceof TextField) {
                TextField tf = (TextField) n;
                int lig = ((int) n.getProperties().get("gridpane-row")) + 1;
                int col = ((int) n.getProperties().get("gridpane-column")) + 1;

                tf.textProperty().bindBidirectional(mc.propositionProperty(lig, col));


                String defH = mc.getDefinition(lig, col, true);
                String defV = mc.getDefinition(lig, col, false);

                if (defH != null && defV != null) {
                    tf.setTooltip(new Tooltip(defH + " / " + defV));
                } else if (defH != null) {
                    tf.setTooltip(new Tooltip(defH));
                } else if (defV != null) {
                    tf.setTooltip(new Tooltip(defV));
                }
                tf.setOnMouseClicked((e) -> {
                    this.clicCase(e);
                });
            }
        }
    }

    private void creerGrille() {
        TextField modele = (TextField) monGridPane.getChildren().get(0);
        monGridPane.getChildren().clear();

        for (int lig = 1; lig <= mc.getHauteur(); lig++) {
            for (int col = 1; col <= mc.getLargeur(); col++) {
                if (!mc.estCaseNoire(lig, col)) {
                    TextField tf = new TextField();
                    tf.setPrefHeight(modele.getPrefHeight());
                    tf.setPrefWidth(modele.getPrefWidth());
                    //tf.setStyle("-fx-border-color: #8dc5e0 ;-fx-border-width: 2px");
                    for (Object cle : modele.getProperties().keySet()) {
                        tf.getProperties().put(cle, modele.getProperties().get(cle));
                    }
                    monGridPane.add(tf, col - 1, lig - 1);
                }
            }
        }
    }

    @FXML
    public void clicCase(MouseEvent e) {
        if (e.getButton() == MouseButton.SECONDARY) {

            TextField maCase = (TextField) e.getSource();
            int lig = ((int) maCase.getProperties().get("gridpane-row")) + 1;
            int col = ((int) maCase.getProperties().get("gridpane-column")) + 1;
            mc.reveler(lig, col);
            maCase.textProperty().setValue(Character.toString(mc.getProposition(lig, col)));
        }
    }

    public void close(MouseEvent e) {
        if (e.getButton() == MouseButton.PRIMARY) {
            System.exit(0);
        }
    }

    public void motAleatoire(MouseEvent e) throws IOException {
        if (e.getButton() == MouseButton.PRIMARY) {
            Stage stage = (Stage) bMot.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader() ;
            loader.setLocation(Main.class.getResource("VueChoix2.fxml"));
            Parent root = (Parent) loader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
            stage.setScene(scene);
            stage.show();
        }
    }

    int choixNumGrille() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir chiffre :");
        int i = sc.nextInt();
        return i ;
    }


//Etc.

}
